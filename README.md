# kiln-builder

This repo contains a script for building Kiln on various
distributions. The chosen distributions are Docker, Debian/Ubuntu and
Virtual Machine (.ova).

The script assumes that the [kiln
repo](https://gitlab.com/tezos-kiln/kiln) is kiln directory is found
at (assuming the current directory is $PWD)
``` shell
$PWD/kiln
```
Before running the script, make sure to make it executable:
``` shell
chmod +x ./releases.hs
```
Upon doing so, there are several ways to run the script. For reference
here is the help page

``` shell
>>> ./releases.hs --help
[...extraneous output...]
A script to build kiln artifacts

Usage: script (-p|--checkout-point BRANCH|REVISION|TAG) [-c|--caching]
              [--kiln-debian] [--dockerImage] [--kilnVM]
  Build all kiln artifacts given some branch|revision|tag

Available options:
  -p,--checkout-point BRANCH|REVISION|TAG
                           A branch, revision or tag to checkout
  -c,--caching             Whether to send builds' closures to s3
                           cache(s3://kiln-nix-binary-cache).
  --kiln-debian            Build kiln-debian
  --dockerImage            Build dockerImage
  --kilnVM                 Build kilnVM
  -h,--help                Show this help text
```
To build kiln, you must first select a branch, revision or tag. Upon
doing so, you can select which Kiln distributions to build. For
example, if you wish to only build the Debian/Ubuntu distribution, do
the following.

``` shell
>>> ./releases.hs -p <some-tag> --kiln-debian
```
If however, you wish to build all distributions simply don't specify
any of the distributions like so:

``` shell
>>> ./releases.hs -p <some-tag>
```
