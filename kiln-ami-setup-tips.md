# Overview

The guide should help you setup an EC2 instance to build kiln from
_mostly_ scratch. From scratch, what we mean here is that you wish to
create a new instance for building kiln artifact(s).

## EC2 instance options

There are several important steps for creating an EC2 instance to run
kiln builds. I shall go through step carefully.

1. Choose an Amazon Machine Image (AMI)

This step is fairly straightforward. We need to use the NixOS AMI. You
can search for this term -- NixOS-20.03pre130979.gfedcba-x86_64-linux.
Although, there are other versions for NixOS, for the sake of consistency
with previous attempts to build kiln, please choose this one.

2. Choose an instance type

In this step, you will need to select an instance type for the previously
selected AMI. This instance type will dictate the CPU types (and the
number of them), the amount of RAM and other important details. Given
the nature of these builds (they are CPU & RAM intensive), you should
select an instance type with at least 8GB of RAM, though 16 GB may be
preferable.

3. Configure Instance Details

In this step, you need to select the default subnet for your instance.
This step is important because the subnet of your EC2 instance needs
to match the subnet of the EBS volumes you will later be attaching. For
instance, in previous runs, the default subnet for us-east-1e was
selected. So please keep note of whatever choice you make for later on
when you create new EBS volumes to attach to this instance OR
when you choose already existing volumes to attach.

4. Add Storage

In this step, you can set the size of the root volume of your machine.
I would recommend setting this no higher than 10 GB. You definitely
may need a little room to install whatever utilities you'd like to use
on the instance before attaching any EBS volumes (e.g. emacs, wget,
etc.).

5. Add Tags

A tag is defined as key-value pair. I would recommended that all keys
 (for these instances) should be kiln. Thereafter, you can set the associated
value to something you deem relevant to this instance.

6. Configure Security Group

In this step, you can choose to use an already existing security group
or create a new one. If you choose to create a new one, just make sure
that port 22 (SSH) is open for whatever IP address (or range) you
would like.


## IAM role

Once you've launced an instance, you'll next need to modify its IAM
role. There is already an existing KilnReleaseManager role that has
the required policies attached (namely AmazonS3FullAccess and
SecretsManagerReadWrite). You should choose this one.

## Attaching EBS Volume

Once you've selected an EBS volume of the desired size, make sure that
the selected availability zone matches the selected subnet from the
"Configure Instance details" from above.

You will need to create EBS volumes corresponding to "/nix"
and "/tmp". The former should be at least 100 GB and the latter
between 20 to 30 GB. The latter volume is needed for the kilnVM build.

### Mounting /nix

This is a crucial step because messing this up means potentially not
having access to such crucial commands like "ls" and "pwd". In this
step, we are going to be making an EBS volume mentioned in the last
step the nix store for the instance. Assuming you attached the volume,
here are the crucial steps:

1. Create a new ext4 partition and mount it over /mnt
2. rsync -aAxv everything from /nix to /mnt
3. bind /mnt/nix to /nix (e.g. using mount). In particular, issue this
   command "mount --bind /mnt/nix /nix".
4. Issue systemctl restart nix-daemon.service

If everything has worked correctly, you should be able to issue "df
-h" and see what you've mounted. If this does not work, you've likely
done something wrong in step 3. Make sure what you're binding over
/nix is what you have previously rsynced in step 2. If you did
make a mistake (and can't use "ls" for example), simply reboot
the instance (the EBS volume will be unmounted) and try again.

### Mounting /tmp

You can follow the same steps as above while also swapping out /nix
for /tmp.

## Building kiln

At this point, you're almost ready to build kiln. You'll need to issue
this command.

```sh
nix-env -iA nixos.git nixos.binutils nixos.ghc nixos.cabal-install nixos.awscli
```

And then you'll need to get kiln itself.
<!-- include section about SSH keys in a bit -->

```sh
git clone git@gitlab.com:tezos-kiln/kiln.git
```

### Getting the releases.hs script

The script for aoctually building release artifacts is within this
repository. You can get on the build machine by issuing this command.

```sh
curl https://gitlab.com/tezos-kiln/kiln-builder/-/raw/master/releases.hs --output releases.hs && chmod +x releases.hs
```

### Build!

At this point, you can issue this command to start building Kiln.

```sh
./releases --checkout-point <branch|revision|tag> --caching
```

You can access the script's help message by issuing the following
command for more information.

```sh
./releases.hs --help
```
