#!/usr/bin/env cabal
{- cabal:
build-depends: aeson
               , attoparsec >= 0.12.0.0 && < 0.14
               , base >= 4.6.0.0 && < 5
               , containers
               , directory
               , nix-derivation >= 1.1.1
               , optparse-applicative
               , process
               , microlens
               , microlens-aeson
               , mtl
               , text
               , temporary
               , vector
-}

{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates #-}

import Control.Monad.Except

import Data.Attoparsec.Text.Lazy (parse, eitherResult)
import Data.Bool
import Data.Char
import Data.Foldable (fold)
import Data.Function
import Data.Functor ((<&>))
import Data.List
import Data.Semigroup ((<>))
import Data.Text (Text)
import Lens.Micro
import GHC.IO.Handle
import Lens.Micro.Aeson
import Options.Applicative
import System.Directory
import System.Exit
import System.IO.Temp
import System.Process
import Text.Printf
import qualified Data.Map as M
import qualified Data.Text as T
import qualified Data.Text.Lazy.IO
import qualified Data.Vector as V
import qualified Nix.Derivation as D

main :: IO ()
main = withCurrentDirectory "kiln" $ do

    Opts tag toCache buildAttributesFlags <- execParser opts

    fmap (either error id) $ runExceptT $ do

      checkoutTag tag

      secret <- getSecretKey

      withSystemTempFile "kiln-builder-secret.txt" $ \privKeyFile fileHandle -> do

        liftIO $ hPutStr fileHandle secret
        liftIO $ hClose fileHandle

        mapM_ (processAttribute tag toCache privKeyFile) buildAttributesFlags

        liftIO $ putStrLn $ finalMessage toCache

  where
    finalMessage True = printf "All builds are done and their closures have been cached in this s3 bucket %s and the artifacts sent here %s." defaultCacheLcation releasesBucket
    finalMessage False = printf "All builds are done and the artifacts have been sent here %s." releasesBucket
    opts = info (optsParser <**> helper)
        (fullDesc <> progDesc "Build all kiln artifacts given some branch|revision|tag" <> header "A script to build kiln artifacts")

data Opts = Opts String Bool [(String, String)] deriving Show

optsParser :: Parser Opts
optsParser =  Opts <$>
   strOption ( long "checkout-point"
     <> short 'p'
     <> metavar "BRANCH|REVISION|TAG"
     <> help "A branch, revision or tag to checkout")
   <*> switch ( long "caching" <> short 'c' <> help (printf "Whether to send builds' closures to s3 cache(%s)." defaultCacheLcation))
   <*> (decideBuilds
   <$> switch ( long "kiln-debian" <> help (printf "Build kiln-debian"))
   <*> switch ( long "dockerImage" <> help (printf "Build dockerImage"))
   <*> switch ( long "kilnVM" <> help (printf "Build kilnVM"))
   )

decideBuilds :: Bool -> Bool -> Bool -> [(String,String)]
decideBuilds False False False = zip (words "kiln-debian dockerImage kilnVM") ["--recursive","","--recursive"]
decideBuilds debian docker vm =
     fold
     [ bool id ((:) ("kiln-debian","--recursive")) debian
     , bool id ((:) ("dockerImage","")) docker
     , bool id ((:) ("kilnVM","--recursive")) vm
     ]
     []

findExecutable' :: FilePath -> IO (Either Error FilePath)
findExecutable' fp = findExecutable fp <&> \case
  Nothing -> Left $ printf "Cannot find executable %s" fp
  Just path -> Right path

gitExecutable :: IO (Either Error FilePath)
gitExecutable = findExecutable' "git"

checkoutTag :: String -> ExceptT Error IO ()
checkoutTag tag = do
   exe <- ExceptT gitExecutable
   runShellCommand exe (words $ printf "checkout %s" tag) (pure . Left) $ \r -> do
     putStrLn $ printf "Checked out tag (%s)" r
     return $ Right ()

runShellCommand :: FilePath -> [String] -> (String -> IO (Either Error a)) -> (String -> IO (Either Error a)) -> ExceptT Error IO a
runShellCommand exe cmdParams failureHandler successHandler = ExceptT $ do
    (ec, stdout, stderr) <- readProcessWithExitCode exe cmdParams ""
    case ec of
      ExitFailure _ -> failureHandler stderr
      ExitSuccess -> successHandler stdout

runShellCommand' :: FilePath -> [String] -> ExceptT Error IO ()
runShellCommand' exe cmdParams = ExceptT $ withCreateProcess pr go
  where
    pr = (proc exe cmdParams) { std_out = CreatePipe }
    go _ Nothing _ _ = pure $ Left "Couldn't connect to std_out stream handle!"
    go _std_in (Just std_out') _std_err _ph = flip fix Nothing $ \loop mstr -> do
        mapM_ putStrLn mstr
        open <- hIsOpen std_out'
        eof <- hIsEOF std_out'
        if open && not eof
           then hGetLine std_out' >>= loop . Just
           else pure $ Right ()

processAttribute :: String -> Bool -> PrivateKeyFile -> (BuildAttribute, String) -> ExceptT Error IO ()
processAttribute tagName caching privKeyFile (buildAttribute, flg) = do

  outPath <- buildBuildAttribute buildAttribute

  liftIO $ putStrLn outPath

  msg <- sendReleaseToS3 outPath tagName flg

  liftIO $ putStrLn msg

  when caching $ sendClosureToS3Bucket outPath privKeyFile

type Error = String

type PrivateKeyFile = FilePath

nixInstantiate :: IO (Either Error FilePath)
nixInstantiate = findExecutable' "nix-instantiate"

nixBuild :: IO (Either Error FilePath)
nixBuild = findExecutable' "nix-build"

nixStore :: IO (Either Error FilePath)
nixStore = findExecutable' "nix-store"

nix :: IO (Either Error FilePath)
nix = findExecutable' "nix"

retrieveBuildClosure :: FilePath -> ExceptT Error IO (V.Vector Text)
retrieveBuildClosure outPath = do
    exe <- ExceptT nixStore
    runShellCommand exe cmdParams failureHandler successHandler
  where
    closureErrorMsg = printf "Cannot compute closure of %s" outPath
    cmdParams = words $ printf "-qR --include-outputs %s" outPath
    failureHandler stderr = pure $ Left $ unlines [closureErrorMsg, stderr]
    successHandler stdout = pure $ Right $ V.fromList (T.lines $ T.pack stdout)

signPathsAndSend :: PrivateKeyFile -> V.Vector Text -> ExceptT Error IO ()
signPathsAndSend privKey = mapM_ (\p -> signPath privKey p >> sendCommand defaultCacheLcation p)

signPath :: PrivateKeyFile -> Text -> ExceptT Error IO ()
signPath privKey path = do
    exe <- ExceptT nix
    runShellCommand exe cmdParams failureHandler successHandler
  where
    cmdParams = words $ printf "sign-paths --key-file %s %s" privKey (T.unpack path)
    failureHandler = pure . Left
    successHandler = pure . Right . const ()

type CacheLocation = String

defaultCacheLcation :: CacheLocation
defaultCacheLcation = "s3://kiln-nix-binary-cache"

sendCommand :: CacheLocation -> Text -> ExceptT Error IO ()
sendCommand cacheLocation path = do
    exe <- ExceptT nix
    runShellCommand exe cmdParams failureHandler successHandler
  where
    cmdParams = words $ printf "copy --to %s %s" cacheLocation path
    failureHandler = pure . Left
    successHandler = pure . Right . const ()

type BuildAttribute = String

buildBuildAttribute :: BuildAttribute -> ExceptT Error IO String
buildBuildAttribute buildAttribute = do
  buildCommand buildAttribute
  getOutPath buildAttribute

-- garbageCollect :: Bool -> ExceptT Error IO ()
-- garbageCollect = \case
--   False -> return ()
--   True -> do
--     exe <- ExceptT nixStore
--     runShellCommand exe ["--gc"] (pure . Left) (pure . Right . const ())

buildCommand :: BuildAttribute -> ExceptT Error IO ()
buildCommand buildAttribute = do
    exe <- ExceptT nixBuild
    runShellCommand' exe cmdParams
  where
    cmdParams = words $ printf "-A %s" buildAttribute

getOutPath :: BuildAttribute -> ExceptT Error IO String
getOutPath buildAttribute = do
    exe <- ExceptT nixInstantiate
    runShellCommand exe cmdParams (pure . Left) outPath
  where
    cmdParams = words $ printf "-A %s" buildAttribute
    outPath std_out' = case lines std_out' of
     [derivationPath] -> do
       doesExist <- doesFileExist derivationPath
       if not doesExist
         then return $ Left $ printf "Cannot find %s" derivationPath
         else do
           text <- Data.Text.Lazy.IO.readFile derivationPath
           let result = D.outputs <$> eitherResult (parse D.parseDerivation text)
               lookup' = M.lookup "out"
           return $ either Left (maybe (Left "Derivation output lookup failed") (Right . D.path) . lookup') result
     _ -> pure $ Left "getOutPath: Something went wrong."

sendClosureToS3Bucket :: FilePath -> PrivateKeyFile -> ExceptT Error IO ()
sendClosureToS3Bucket outPath privKey = do
  closurePaths <- retrieveBuildClosure outPath
  signPathsAndSend privKey closurePaths

awsExe :: IO (Either Error FilePath)
awsExe = findExecutable' "aws"

sendReleaseToS3 :: FilePath -> String -> String -> ExceptT Error IO String
sendReleaseToS3 outPath tagName flg = do
    aws <- ExceptT awsExe
    exists <- checkIfExistsInS3 s3name
    if not exists
      then runShellCommand aws cmdParams (pure . Left) (pure . Right)
      else return (printf "Don't send the same build (%s) more than once." s3name)
  where
    s3name = releasesBucket ++ "/" ++ stripSpaces tagName ++ "/" ++ drop 11 outPath
    cmdParams = words $ printf "s3 cp %s %s %s" outPath s3name flg

checkIfExistsInS3 :: String -> ExceptT Error IO Bool
checkIfExistsInS3 s3name = ExceptT $ do
    eaws <- awsExe
    case eaws of
       Left err -> pure $ Left err
       Right aws -> do
           response <- runExceptT $ runShellCommand aws cmdParams (pure . Left) (pure . Right . not . null)
           pure $ Right $ either (const False) id response
  where
    cmdParams = words $ printf "s3 ls %s" s3name

stripSpaces :: String -> String
stripSpaces = dropWhileEnd isSpace . dropWhile isSpace

releasesBucket :: FilePath
releasesBucket = "s3://kiln-releases"

getSecretKey :: ExceptT Error IO String
getSecretKey = do
    aws <- ExceptT awsExe
    runShellCommand aws cmdParams (pure . Left) (pure . fmap T.unpack . getSecret)
  where
    cmdParams = words "secretsmanager get-secret-value --secret-id kiln-nix-binary-cache-key --version-stage AWSCURRENT"

    getSecret res = do
        let json = res & (^? (key "SecretString" . _String))
        note' $ json >>= (^? key "cache-private-key" . _String)

    note' = maybe (Left "getSecretKey: json didn't parse") Right
